/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive

import groovy.transform.CompileStatic
import java.nio.charset.StandardCharsets
import java.nio.file.Path
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

@CompileStatic
abstract class ArchiveMakerSpec extends Specification {

  static final String UTF_8 = StandardCharsets.UTF_8.name()

  @Rule
  TemporaryFolder temporaryFolder = new TemporaryFolder()

  Path inputsDir
  Path outputDir

  void setup() {
    inputsDir = temporaryFolder.newFolder("input").toPath()
    outputDir = temporaryFolder.newFolder("output").toPath()
  }

  // Create a test file with the given name - the file's content will also be the name so that we can recognize it
  //   for comparisons
  Path createTestFile(String name) {
    def path = inputsDir.resolve(name)
    path.setText(name, UTF_8)
    return path
  }
}
