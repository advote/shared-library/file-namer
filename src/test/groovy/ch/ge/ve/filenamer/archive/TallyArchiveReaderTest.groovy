/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive


import java.time.LocalDateTime
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification
import spock.util.mop.Use

@Use(ZipHelper)
class TallyArchiveReaderTest extends Specification {

  private static final String UTF_8 = "UTF-8"

  @Rule
  TemporaryFolder temporaryFolder = new TemporaryFolder()

  private List<String> expectedFilenames = [
          "ballots.json",
          "confirmations.json",
          "counting-circles.json",
          "election-set-for-verification.json",
          "generators.json",
          "partial-decryption-proofs.json",
          "partial-decryptions.json",
          "primes.json",
          "public-credentials.json",
          "public-key-parts.json",
          "public-parameters.json",
          "shuffle-proofs.json",
          "shuffles.json",
          "operation_ref.xml"
  ]

  def "should read the metadata from archive name"() {
    given:
    def archive = temporaryFolder.newFile("tally-archive_test-operation_2018-08-06-11h44m10s.taf").toPath()
    archive.zip(expectedFilenames)

    when:
    TallyArchiveReader reader = TallyArchiveReader.readFrom(archive)

    then:
    reader.operationName == "test operation"
    reader.creationDate.get() == LocalDateTime.of(2018, 8, 6, 11, 44, 10)
  }

  def "should fail when given an invalid archive file"() {
    given:
    def archive = temporaryFolder.newFile("tally-archive_test-operation_2018-08-06-11h44m10s.taf").toPath()
    archive.zip(["file_a.xml", "file_b.json"])

    when:
    TallyArchiveReader.withArchive(archive, {})

    then:
    thrown InvalidArchiveException
  }

  def "should provide access to its content via appropriately named getter methods"() {
    given:
    def archive = temporaryFolder.newFile("tally-archive_test-operation.taf").toPath()
    archive.zip([
            "ballots.json" : "The ballots",
            "confirmations.json" : "The confirmations",
            "counting-circles.json" : "number of voters per CC and per DOI",
            "election-set-for-verification.json" : "The election-set without keys",
            "generators.json" : "The generators",
            "partial-decryption-proofs.json" : "Some partial decryption proofs",
            "partial-decryptions.json" : "Partial decryption data",
            "primes.json" : "Some prime numbers",
            "public-credentials.json" : "The public credentials",
            "public-key-parts.json" : "The public key parts",
            "public-parameters.json" : '{"public": "parameters"}',
            "shuffle-proofs.json" : "The proofs the shuffle has been done",
            "shuffles.json" : "The shuffle data",
            "eCH-0157.xml": "The election reference",
            "eCH-0159.xml": "The votation reference"
    ], UTF_8)

    when:
    TallyArchiveReader reader = TallyArchiveReader.readFrom(archive)

    then:
    reader.ballotsSource.getText(UTF_8) == "The ballots"
    reader.confirmationsSource.getText(UTF_8) == "The confirmations"
    reader.countingCirclesSource.getText(UTF_8) == "number of voters per CC and per DOI"
    reader.electionSetForVerificationSource.getText(UTF_8) == 'The election-set without keys'
    reader.generatorsSource.getText(UTF_8) == "The generators"
    reader.partialDecryptionProofsSource.getText(UTF_8) == "Some partial decryption proofs"
    reader.partialDecryptionsSource.getText(UTF_8) == "Partial decryption data"
    reader.primesSource.getText(UTF_8) == "Some prime numbers"
    reader.publicCredentialsSource.getText(UTF_8) == "The public credentials"
    reader.publicKeyPartsSource.getText(UTF_8) == "The public key parts"
    reader.publicParameters.getText(UTF_8) == '{"public": "parameters"}'
    reader.shuffleProofsSource.getText(UTF_8) == "The proofs the shuffle has been done"
    reader.shufflesSource.getText(UTF_8) == "The shuffle data"
    reader.operationReferenceSources.collectEntries {[it.key, it.value.getText(UTF_8)]} == [
            "eCH-0157.xml": "The election reference",
            "eCH-0159.xml": "The votation reference"
    ]
  }

  def "an archive with no operation references should not be valid"() {
    given:
    def archive = temporaryFolder.newFile("tally-archive_test-operation.taf").toPath()
    archive.zip(expectedFilenames - ["operation_ref.xml"])

    when:
    TallyArchiveReader.withArchive(archive, {})

    then:
    def ex = thrown InvalidArchiveException
    ex.message == 'No files were found for some expected groups : [operation reference]'
  }

  def "should give access to all operation reference entries names"() {
    def archive = temporaryFolder.newFile("tally-archive_test-operation.taf").toPath()
    archive.zip(expectedFilenames + ["an_other_operation_ref.xml"])

    when:
    TallyArchiveReader reader = TallyArchiveReader.readFrom(archive)

    then:
    reader.getOperationReferenceNames() == ["operation_ref.xml", "an_other_operation_ref.xml"]
  }

  def "operation reference entries should be accessible by their name"() {
    def archive = temporaryFolder.newFile("tally-archive_test-operation.taf").toPath()
    archive.zip([
            "ballots.json" : "The ballots",
            "confirmations.json" : "The confirmations",
            "counting-circles.json" : "number of voters per CC and per DOI",
            "election-set-for-verification.json" : "The election-set without keys",
            "generators.json" : "The generators",
            "partial-decryption-proofs.json" : "Some partial decryption proofs",
            "partial-decryptions.json" : "Partial decryption data",
            "primes.json" : "Some prime numbers",
            "public-credentials.json" : "The public credentials",
            "public-key-parts.json" : "The public key parts",
            "public-parameters.json" : '{"public": "parameters"}',
            "shuffle-proofs.json" : "The proofs the shuffle has been done",
            "shuffles.json" : "The shuffle data",
            "eCH-0157.xml": "The election reference",
            "eCH-0159.xml": "The votation reference"
    ], UTF_8)

    when:
    TallyArchiveReader reader = TallyArchiveReader.readFrom(archive)

    then: 'one can read each reference independently'
    reader.getOperationReferenceSource("eCH-0157.xml").text == "The election reference"
    reader.getOperationReferenceSource("eCH-0159.xml").text == "The votation reference"

    when: 'querying for a non-operation-reference entry'
    reader.getOperationReferenceSource("ballots.json")

    then:
    thrown IllegalArgumentException
  }
}
