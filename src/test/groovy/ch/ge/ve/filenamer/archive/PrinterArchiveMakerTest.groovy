/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive


import java.time.LocalDateTime

class PrinterArchiveMakerTest extends ArchiveMakerSpec {

  // Prepare a PrinterArchiveMaker, setting all files using "createTestFile(..)"
  def preparePrinterArchiveMaker(String printerName) {
    return new PrinterArchiveMaker(printerName)
            .withVotersReferenceSource(createTestFile("voters.xml"))
            .withPublicParametersSource(createTestFile("my-public-params.json"))
            .withElectionSetSource(createTestFile("my-election-set.json"))
            .withOperationConfigurationSource(createTestFile("my-op-conf.json"))
            .withElectionConfigurationsSource(createTestFile("my-election-conf.json"))
            .withOperationReferenceSources([ createTestFile("xx_operation.xml") ])
            .withPrivateCredentialsSources([ createTestFile("xx_printer-file.epf") ])
  }

  def "should create a complete archive with all the specified inputs"() {
    given: 'archive metadata'
    def printer = "test printer"
    def operation = "Unit test operation"
    def creationDate = LocalDateTime.of(2018, 8, 3, 17, 59)

    and: 'a Maker object prepared with simple files'
    def printerArchiveMaker = preparePrinterArchiveMaker(printer)
            .withOperationName(operation)
            .withCreationDate(creationDate)

    when:
    def archive = printerArchiveMaker.make(outputDir)

    then: 'the archive should have been created with the appropriate name'
    archive.fileName as String == 'printer-archive_Unit-test-operation_test-printer_2018-08-03-17h59m00s.paf'

    and: 'the archive should be a ZIP that contains all expected files with original data'
    ZipHelper.readEntries(archive, UTF_8) == [
            "eCH-0045.xml": "voters.xml",
            "public-parameters.json": "my-public-params.json",
            "election-set.json": "my-election-set.json",
            "operation-configuration.json": "my-op-conf.json",
            "election-configurations.json": "my-election-conf.json",
            "xx_operation.xml": "xx_operation.xml",
            "xx_printer-file.epf": "xx_printer-file.epf",
    ]
  }

  def "OperationReference sources should be appendable one by one"() {
    given:
    def printerArchiveMaker = new PrinterArchiveMaker("printer")

    when:
    printerArchiveMaker.addOperationReferenceSource(createTestFile("op1.xml"))
    printerArchiveMaker.addOperationReferenceSource(createTestFile("op2.xml"))

    then:
    printerArchiveMaker.@operationReferenceSources.collect { it.getFileName().toString() } == [
            "op1.xml", "op2.xml"
    ]
  }

  def "OperationReference Sources require .xml extension files"() {
    given:
    def jsonFile = inputsDir.resolve("operation-reference.json")

    when:
    new PrinterArchiveMaker("printer").withOperationReferenceSources([jsonFile])

    then:
    def ex = thrown IllegalArgumentException
    ex.message == '<operation reference> sources are required to have a ".xml" extension, but found : "operation-reference.json"'
  }

  def "PrivateCredentials Sources require .epf extension files"() {
    given:
    def xmlFile = inputsDir.resolve("printer-file.xml")

    when:
    new PrinterArchiveMaker("printer").withPrivateCredentialsSources([xmlFile])

    then:
    def ex = thrown IllegalArgumentException
    ex.message == '<private credentials> sources are required to have a ".epf" extension, but found : "printer-file.xml"'
  }

  def "PrivateCredentials sources should be appendable one by one"() {
    given:
    def printerArchiveMaker = new PrinterArchiveMaker("printer")

    when:
    printerArchiveMaker.addPrivateCredentialsSource(createTestFile("file1.epf"))
    printerArchiveMaker.addPrivateCredentialsSource(createTestFile("file2.epf"))

    then:
    printerArchiveMaker.@privateCredentialsSources.collect { it.getFileName().toString() } == [
            "file1.epf", "file2.epf"
    ]
  }

  def "should fail if a resource is missing"() {
    given: 'a Maker prepared with missing public parameters'
    def printerArchiveMaker = new PrinterArchiveMaker("printer").withOperationName("operation")
            .withCreationDate(LocalDateTime.now())
            .withVotersReferenceSource(createTestFile("voters.xml"))
            .withElectionSetSource(createTestFile("my-election-set.json"))
            .withOperationConfigurationSource(createTestFile("my-op-conf.json"))
            .withElectionConfigurationsSource(createTestFile("my-election-conf.json"))
            .withOperationReferenceSources([ createTestFile("xx_operation.xml") ])
            .withPrivateCredentialsSources([ createTestFile("xx_printer-file.epf") ])

    when:
    printerArchiveMaker.make(temporaryFolder.newFolder().toPath())

    then:
    def ex = thrown InvalidArchiveException
    ex.message == 'public-parameters.json has not been specified.'
  }

  def "should fail if operation references are missing"() {
    given: 'a Maker object prepared with simple files but no operation reference'
    def printerArchiveMaker = new PrinterArchiveMaker("printer").withOperationName("operation")
            .withCreationDate(LocalDateTime.now())
            .withVotersReferenceSource(createTestFile("voters.xml"))
            .withPublicParametersSource(createTestFile("my-public-params.json"))
            .withElectionSetSource(createTestFile("my-election-set.json"))
            .withOperationConfigurationSource(createTestFile("my-op-conf.json"))
            .withElectionConfigurationsSource(createTestFile("my-election-conf.json"))
            .withPrivateCredentialsSources([ createTestFile("xx_printer-file.epf") ])

    when:
    printerArchiveMaker.make(temporaryFolder.newFolder().toPath())

    then:
    def ex = thrown InvalidArchiveException
    ex.message == 'Operation references (eCH-157 / eCH-159) have not been specified.'
  }

  def "should fail if private credentials are missing"() {
    given: 'a Maker object prepared with simple files but no operation reference'
    def printerArchiveMaker = new PrinterArchiveMaker("printer").withOperationName("operation")
            .withCreationDate(LocalDateTime.now())
            .withVotersReferenceSource(createTestFile("voters.xml"))
            .withPublicParametersSource(createTestFile("my-public-params.json"))
            .withElectionSetSource(createTestFile("my-election-set.json"))
            .withOperationConfigurationSource(createTestFile("my-op-conf.json"))
            .withElectionConfigurationsSource(createTestFile("my-election-conf.json"))
            .withOperationReferenceSources([ createTestFile("xx_operation.xml") ])

    when:
    printerArchiveMaker.make(temporaryFolder.newFolder().toPath())

    then:
    def ex = thrown InvalidArchiveException
    ex.message == 'Private credentials (epf) have not been specified.'
  }

  def "should fail if the generated archive name does not match the pattern"() {
    given: 'a non-conform printer name'
    def printerName = "Printer #1"

    when:
    preparePrinterArchiveMaker(printerName).withOperationName("op1")
            .make(temporaryFolder.newFolder().toPath())

    then:
    def ex = thrown IllegalArgumentException
    ex.message == 'The name "printer-archive_op1_Printer-#1.paf" does not match the expected pattern for a PrinterArchive'
  }

  def "should fail if output is not a directory"() {
    when:
    preparePrinterArchiveMaker("printer").withOperationName("operation")
            .make(temporaryFolder.newFile().toPath())

    then:
    thrown IllegalArgumentException
  }
}
