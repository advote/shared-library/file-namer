/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive

import groovy.transform.CompileStatic
import java.nio.file.Path
import java.util.zip.ZipEntry
import java.util.zip.ZipFile
import java.util.zip.ZipOutputStream

@CompileStatic
class ZipHelper {

  private ZipHelper() { /* Not instantiable */ }

  /**
   * Creates a ZIP archive, each name in the list being an (empty) entry in the archive
   */
  static void zip(Path zipPath, List<String> fileNames) {
    Map<String, String> entriesWithEmptyContent = fileNames.collectEntries { [it, null] }
    zip(zipPath, entriesWithEmptyContent, null)
  }

  /**
   * Creates a ZIP archive, each entry in the map being an entry in the archive with the associated textual content
   */
  static void zip(Path zipPath, Map<String, String> entryWithContent, String charset) {
    zipPath.toFile().withOutputStream {
      def zipOut = new ZipOutputStream(it)

      entryWithContent.each { name, content ->
        zipOut.putNextEntry(new ZipEntry(name))
        if (content) {
          zipOut.write(content.getBytes(charset))
        }
        zipOut.closeEntry()
      }
      zipOut.close()
    }
  }

  /**
   * Read all zip entries and return them as a Map [ "entry_name" : "content" ]
   */
  static Map<String, String> readEntries(Path zipPath, String charset) {
    return new ZipFile(zipPath.toFile()).withCloseable { ZipFile zipFile ->

      zipFile.entries().collect { ZipEntry entry ->
        def name = entry.name
        def content = zipFile.getInputStream(entry).getText(charset)
        return [name, content]

      }.collectEntries()
    }
  }
}
