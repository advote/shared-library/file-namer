/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer

import spock.lang.Specification

class CaseFormatTest extends Specification {

  def "ToKebabCase should separate words with an hyphen (-)"() {
    expect:
    CaseFormat.toKebabCase("one two three") == 'one-two-three'
  }

  def "ToKebabCase should escape hyphens in original String by doubling it"() {
    expect:
    CaseFormat.toKebabCase("left-right") == 'left--right'
  }

  def "ToKebabCase does not support the '_' character"() {
    when:
    CaseFormat.toKebabCase("snake_case_string")

    then:
    thrown(IllegalArgumentException)
  }

  def "FromKebabCase should separate words by hyphens"() {
    expect:
    CaseFormat.fromKebabCase('one-two-three') == 'one two three'
  }

  def "FromKebabCase should 'un-escape' doubled hyphens"() {
    expect:
    CaseFormat.fromKebabCase('left--right') == 'left-right'
  }

  def "FromKebabCase should distinguished doubled and single hyphens in the same source"() {
    expect:
    CaseFormat.fromKebabCase('left--right-was-doubled') == 'left-right was doubled'
  }
}
