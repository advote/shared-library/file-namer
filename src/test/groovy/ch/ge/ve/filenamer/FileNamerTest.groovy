/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer

import java.time.LocalDateTime
import spock.lang.Specification

class FileNamerTest extends Specification {

  LocalDateTime creationDate = LocalDateTime.of(2018, 9, 23, 14, 1)

  def 'eCH228Archive'() {
    when:
    def fileName = FileNamer.ech228Archive('201809VP', 'BOH', creationDate)

    then:
    fileName == 'eCH-0228-archive_201809VP_BOH_2018-09-23-14h01m00s.zip'
  }

  def 'lotPrinterFile'() {
    expect:
    FileNamer.lotPrinterFile(printerArchiveType, operationName, lotName, printerName, creationDate) == fileName

    where:
    printerArchiveType                 | operationName          | printerName | lotName    || fileName
    PrintingLotType.TEST_NOT_PRINTABLE | '201809VP'             | 'BOH'       | 'main lot' || 'cards-test-not-printable_201809VP_BOH_main-lot_2018-09-23-14h01m00s'
    PrintingLotType.TEST_PRINTABLE     | 'short name'           | 'BOH'       | 'mainlot'  || 'cards-test-printable_short-name_BOH_mainlot_2018-09-23-14h01m00s'
    PrintingLotType.PRINTER            | 'slightly longer name' | 'BOH'       | 'main lot' || 'cards-printer-printable_slightly-longer-name_BOH_main-lot_2018-09-23-14h01m00s'
    PrintingLotType.CONTROL            | '201809VP'             | 'BOH'       | 'With MAJ' || 'cards-control-printable_201809VP_BOH_With-MAJ_2018-09-23-14h01m00s'
  }

  def 'eCH-0110 result file'() {
    when:
    def fileName = FileNamer.ech110File('201809VP', creationDate)

    then:
    fileName == 'eCH-0110_201809VP_2018-09-23-14h01m00s.xml'
  }

  def 'eCH-0222 result file'() {
    when:
    def fileName = FileNamer.ech222File('201809VP', creationDate)

    then:
    fileName == 'eCH-0222_201809VP_2018-09-23-14h01m00s.xml'
  }

  def 'eCH-0228 printer file'() {
    when:
    def fileName = FileNamer.ech228File('201809VP', 'Printer 1', creationDate)

    then:
    fileName == 'eCH-0228_201809VP_Printer-1_2018-09-23-14h01m00s.xml'
  }

  def 'votersReport'() {
    when:
    def fileName = FileNamer.votersReport('201809VP', 'register 1', creationDate)

    then:
    fileName == 'voters-report_201809VP_register-1_2018-09-23-14h01m00s.pdf'
  }

  def 'consolidatedVotersReport'() {
    when:
    def fileName = FileNamer.consolidatedVotersReport('201809VP', creationDate)

    then:
    fileName == 'consolidated-voters-report_201809VP_2018-09-23-14h01m00s.pdf'
  }

}
