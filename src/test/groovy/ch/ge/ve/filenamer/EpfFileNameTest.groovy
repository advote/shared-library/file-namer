/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer

import java.nio.file.Path
import java.nio.file.Paths
import nl.jqno.equalsverifier.EqualsVerifier
import spock.lang.Specification

class EpfFileNameTest extends Specification {

  def "it must require non-null inputs"() {
    when:
    code.call()

    then:
    thrown(NullPointerException)

    where:
    code << [
        { _ -> EpfFileName.parse(null as Path) },
        { _ -> EpfFileName.parse(null as String) },
        { _ -> new EpfFileName(null, 0, '0-4') },
        { _ -> new EpfFileName('printer1', 0, null) },
    ]
  }

  def "it must validate its inputs"() {
    when:
    code.call()

    then:
    thrown(IllegalArgumentException)

    where:
    code << [
        { _ -> EpfFileName.parse(Paths.get('#invalid#.zip')) },
        { _ -> EpfFileName.parse('#invalid#.zip') },
        { _ -> new EpfFileName('#invalid#', 0, '0-4') },
        { _ -> new EpfFileName('printer1', 0, '#invalid#') },
    ]
  }

  def "it must generate valid file names"() {
    expect:
    new EpfFileName('printer1', 0, '0-2').toString() == 'printer1_CC0_0-2.epf'
  }

  def "it must successfully parse valid file names"() {
    expect:
    def fileName = EpfFileName.parse('printer1_CC0_0-2.epf')
    fileName.printingAuthorityName == 'printer1'
    fileName.controlComponentIndex == 0
    fileName.qualifier == '0-2'
  }

  def "it must successfully parse file names it has generated"() {
    expect:
    EpfFileName.parse(new EpfFileName(printerName, ccIndex, qualifier).toPath()).printingAuthorityName == printerName
    EpfFileName.parse(new EpfFileName(printerName, ccIndex, qualifier).toPath()).controlComponentIndex == ccIndex
    EpfFileName.parse(new EpfFileName(printerName, ccIndex, qualifier).toPath()).qualifier == qualifier

    where:
    printerName  | ccIndex | qualifier
    'printer--1' | 0       | '00-10'
    'printer-02' | 1       | '10-20'
    'printer--3' | 2       | '20-30'
    'printer-04' | 3       | '30-40'
  }

  def "it must override equals() and hashCode()"() {
    expect:
    EqualsVerifier.forClass(EpfFileName).withOnlyTheseFields("fileName").verify();
  }
}
