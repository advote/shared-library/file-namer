/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer;

/**
 * A class that centralizes the case conventions we use.
 */
public class CaseFormat {

  private static final char ILLEGAL_CHARACTER = '_';

  private CaseFormat() {
    throw new AssertionError("Not instantiable");
  }

  /**
   * Rewrite a String by separating words with hyphens : '-'
   * <p>
   *   Please note that the '_' is forbidden as it is used for the words separation at the filename-level.
   *   Already present hyphens will be doubled in the resulting string.
   * </p>
   *
   * @param original the string to convert
   * @return the kebab-case version of the string
   */
  public static String toKebabCase(String original) {
    if (original.indexOf(ILLEGAL_CHARACTER) != -1) {
      throw new IllegalArgumentException(String.format("Character '%s' is not allowed in \"%s\"",
                                                       ILLEGAL_CHARACTER, original));
    }
    return original.replaceAll("-", "--").replaceAll(" ", "-");
  }

  /**
   * Rewrite a kebab-case version of a string as space-separated words.
   *
   * @param source a string in a kebab-case convention
   * @return the string reverted to its original form with space-separated words
   */
  public static String fromKebabCase(String source) {
    if (source.contains("--")) {
      String tmpReplacement = String.valueOf(ILLEGAL_CHARACTER);
      return source.replaceAll("--", tmpReplacement)
                   .replaceAll("-", " ")
                   .replaceAll(tmpReplacement, "-");
    } else {
      return source.replaceAll("-", " ");
    }
  }

}
