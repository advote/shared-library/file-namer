/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive;

import static java.util.Objects.requireNonNull;

import ch.ge.ve.filenamer.TallyArchiveFileName;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This object will assist in creating a valid tally archive artifact.
 * <p>
 *   Using this component ensures that a successfully created archive will be correctly read by the corresponding
 *   reader, by encapsulating the contract of what the archive contains.
 * </p>
 * <p>
 *   This class offers a builder-like API, so that all the necessary sources can be defined when known and in
 *   a readable way. When all resources have been declared using the {@code with...} methods, a simple call to
 *   {@link #make(Path)} will create the archive, failing if anything is missing or inconsistent.
 * </p>
 */
public class TallyArchiveMaker {

  private final String        operationName;
  private       LocalDateTime creationDate;

  private Path       ballots;
  private Path       confirmations;
  private Path       countingCircles;
  private Path       electionSetForVerification;
  private Path       generators;
  private Path       partialDecryptionProofs;
  private Path       partialDecryptions;
  private Path       primes;
  private Path       publicCredentials;
  private Path       publicKeyParts;
  private Path       publicParameters;
  private Path       shuffleProofs;
  private Path       shuffles;
  private List<Path> operationReferenceSources = new ArrayList<>();

  /**
   * Instantiate a new maker object.
   *
   * @param operationName the name of the operation this maker is for
   */
  public TallyArchiveMaker(String operationName) {
    this.operationName = requireNonNull(operationName, "Null operationName not allowed");
  }

  /**
   * Define the creationDate. This parameter is optional, {@code LocalDateTime.now()} will be used if not set.
   *
   * @param creationDate the creation date to indicate for this archive
   */
  public TallyArchiveMaker withCreationDate(LocalDateTime creationDate) {
    this.creationDate = requireNonNull(creationDate, "Null creationDate not allowed");
    return this;
  }

  /**
   * Define the ballots source.
   *
   * @param ballotsSource the source for the "ballots" in the protocol model
   */
  public TallyArchiveMaker withBallotsSource(Path ballotsSource) {
    this.ballots = requireNonNull(ballotsSource);
    return this;
  }

  /**
   * Define the confirmations source.
   *
   * @param confirmationsSource the source for the "confirmations" in the protocol model
   */
  public TallyArchiveMaker withConfirmationsSource(Path confirmationsSource) {
    this.confirmations = requireNonNull(confirmationsSource);
    return this;
  }

  /**
   * Define the countingCircles source.
   *
   * @param countingCirclesSource the source for the number of voters per counting circle and per DOI
   */
  public TallyArchiveMaker withCountingCirclesSource(Path countingCirclesSource) {
    this.countingCircles = requireNonNull(countingCirclesSource);
    return this;
  }

  /**
   * Define the electionSetForVerification source.
   *
   * @param electionSetForVerificationSource the source for the "election-set" in the protocol verification model
   */
  public TallyArchiveMaker withElectionSetForVerificationSource(Path electionSetForVerificationSource) {
    this.electionSetForVerification = requireNonNull(electionSetForVerificationSource);
    return this;
  }

  /**
   * Define the generators source.
   *
   * @param generatorsSource the source for the "generators" in the protocol model
   */
  public TallyArchiveMaker withGeneratorsSource(Path generatorsSource) {
    this.generators = requireNonNull(generatorsSource);
    return this;
  }

  /**
   * Define the partialDecryptionProofs source.
   *
   * @param partialDecryptionProofsSource the source for the "partialDecryptionProofs" in the protocol model
   */
  public TallyArchiveMaker withPartialDecryptionProofsSource(Path partialDecryptionProofsSource) {
    this.partialDecryptionProofs = requireNonNull(partialDecryptionProofsSource);
    return this;
  }

  /**
   * Define the partialDecryptions source.
   *
   * @param partialDecryptionsSource the source for the "partialDecryptions" in the protocol model
   */
  public TallyArchiveMaker withPartialDecryptionsSource(Path partialDecryptionsSource) {
    this.partialDecryptions = requireNonNull(partialDecryptionsSource);
    return this;
  }

  /**
   * Define the primes source.
   *
   * @param primesSource the source for the "primes" in the protocol model
   */
  public TallyArchiveMaker withPrimesSource(Path primesSource) {
    this.primes = requireNonNull(primesSource);
    return this;
  }

  /**
   * Define the publicCredentials source.
   *
   * @param publicCredentialsSource the source for the "publicCredentials" in the protocol model
   */
  public TallyArchiveMaker withPublicCredentialsSource(Path publicCredentialsSource) {
    this.publicCredentials = requireNonNull(publicCredentialsSource);
    return this;
  }

  /**
   * Define the publicKeyParts source.
   *
   * @param publicKeyPartsSource the source for the "publicKeyParts" in the protocol model
   */
  public TallyArchiveMaker withPublicKeyPartsSource(Path publicKeyPartsSource) {
    this.publicKeyParts = requireNonNull(publicKeyPartsSource);
    return this;
  }

  /**
   * Define the publicParameters source.
   *
   * @param publicParametersSource the source for the "public parameters" in the protocol model
   */
  public TallyArchiveMaker withPublicParametersSource(Path publicParametersSource) {
    this.publicParameters = requireNonNull(publicParametersSource);
    return this;
  }

  /**
   * Define the shuffleProofs source.
   *
   * @param shuffleProofsSource the source for the "shuffleProofs" in the protocol model
   */
  public TallyArchiveMaker withShuffleProofsSource(Path shuffleProofsSource) {
    this.shuffleProofs = requireNonNull(shuffleProofsSource);
    return this;
  }

  /**
   * Define the shuffles source.
   *
   * @param shufflesSource the source for the "shuffles" in the protocol model
   */
  public TallyArchiveMaker withShufflesSource(Path shufflesSource) {
    this.shuffles = requireNonNull(shufflesSource);
    return this;
  }

  /**
   * Define all operationReference sources. Also known as "eCH-0157" / "eCH-0159".
   * <p>
   *   All files must have a ".xml" extension, as they are stored with their original names and the extension will
   *   be used to distinguished them.
   * </p>
   * <p>
   *   Each call to this method will completely replace all operation references by the supplied ones. Note that if
   *   any of the path does not end with the right extension, the method will fail and be reverted.
   * </p>
   *
   * @param operationReferenceSources the reference sources for the votes (eCH)
   */
  public TallyArchiveMaker withOperationReferenceSources(Collection<? extends Path> operationReferenceSources) {
    this.operationReferenceSources = operationReferenceSources.stream()
                                                              .peek(Internals::verifyOperationReference)
                                                              .collect(Collectors.toList());
    return this;
  }

  /**
   * Add an operationReference source to the currently registered ones. Also known as "eCH-0157" / "eCH-0159".
   *
   * @param operationReferenceSource the reference source for the votes (eCH - xml) to add
   * @see #withOperationReferenceSources(Collection)
   */
  public TallyArchiveMaker addOperationReferenceSource(Path operationReferenceSource) {
    this.operationReferenceSources.add(Internals.verifyOperationReference(operationReferenceSource));
    return this;
  }

  private String getArchiveName() {
    return new TallyArchiveFileName(operationName, creationDate).toString();
  }

  /**
   * Do create the artifact that will serve as the "tally archive".
   * <p>
   *   This method can be called when all properties have been set. Only then will the actual archive file
   *   be created, and all previously defined paths will be streamed into it.
   * </p>
   * <p>
   *   Please note that the file name should not be changed if you want to be able to consistently interpret
   *   the archive's metadata using the corresponding reader.
   * </p>
   *
   * @param outputDirectory the directory where the archive should be created
   * @return the path to the archive artifact
   *
   * @throws IllegalArgumentException if the given outputDirectory is not a directory
   * @throws NullPointerException if any required value is missing
   * @throws UncheckedIOException if an I/O error occurs
   */
  public Path make(Path outputDirectory) {
    final Archive archive = new Archive(getArchiveName());
    archive.append(ballots, Internals.BALLOTS);
    archive.append(confirmations, Internals.CONFIRMATIONS);
    archive.append(countingCircles, Internals.COUNTING_CIRCLES);
    archive.append(electionSetForVerification, Internals.ELECTION_SET_FOR_VERIFICATION);
    archive.append(generators, Internals.GENERATORS);
    archive.append(partialDecryptionProofs, Internals.PARTIAL_DECRYPTION_PROOFS);
    archive.append(partialDecryptions, Internals.PARTIAL_DECRYPTIONS);
    archive.append(primes, Internals.PRIMES);
    archive.append(publicCredentials, Internals.PUBLIC_CREDENTIALS);
    archive.append(publicKeyParts, Internals.PUBLIC_KEY_PARTS);
    archive.append(publicParameters, Internals.PUBLIC_PARAMETERS);
    archive.append(shuffleProofs, Internals.SHUFFLE_PROOFS);
    archive.append(shuffles, Internals.SHUFFLES);
    archive.append(operationReferenceSources, "Operation references (eCH-157 / eCH-159)");

    return archive.create(outputDirectory);
  }
}
