/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer;

import static java.util.Objects.requireNonNull;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents a "printer archive" filename.
 * <p>
 *   A {@code PrinterArchiveFileName} object holds all references to the contained information.
 *   The class provides methods to format and parse file names from the detailed information it should contain.
 * </p>
 */
public final class PrinterArchiveFileName {

  public static final String EXTENSION = "paf";

  // Note : we don't use the "\w" class because we don't accept "_" as a word's character
  private static final Pattern PATTERN = Pattern.compile(
      "printer-archive_([[a-zA-Z0-9]\\-]+)_([[a-zA-Z0-9]\\-]+)(_(\\d{4}-\\d{2}-\\d{2}-\\d{2}h\\d{2}m\\d{2}s))?\\."
      + EXTENSION);

  private final String        printerName;
  private final String        operationName;
  private final LocalDateTime creationDate;

  private final String fileName;

  /**
   * Creates a {@code PrinterArchiveFileName} object from all metadata.
   * <p>
   *   Note that underscores ({@code "_"}) are not allowed in both printer or operation names.
   * </p>
   *
   * @param printerName name of the printer the archive is intended to
   * @param operationName name of the operation this archive is about
   * @param creationDate (optional) when the archive has been created
   *
   * @throws NullPointerException if a non-optional argument is null
   * @throws IllegalArgumentException if the given arguments lead to a invalid filename,
   *                ie one that does not match the expected format
   */
  public PrinterArchiveFileName(String printerName, String operationName, LocalDateTime creationDate) {
    this.printerName = requireNonNull(printerName, "Printer name must be specified");
    this.operationName = requireNonNull(operationName, "Operation name must be specified");
    this.creationDate = creationDate;

    if (creationDate == null) {
      this.fileName = format(CaseFormat.toKebabCase(this.operationName), CaseFormat.toKebabCase(this.printerName));
    } else {
      this.fileName = format(CaseFormat.toKebabCase(this.operationName), CaseFormat.toKebabCase(this.printerName),
                             creationDate.format(FileNamer.DATE_TIME_FORMATTER));
    }

    if ( ! PATTERN.matcher(this.fileName).matches()) {
      throw new IllegalArgumentException(
          "The name \"" + fileName + "\" does not match the expected pattern for a PrinterArchive");
    }
  }

  /**
   * Creates a {@code PrinterArchiveFileName} object from all mandatory metadata.
   * <p>
   *   Note that underscores ({@code "_"}) are not allowed in both printer or operation names.
   * </p>
   *
   * @param printerName name of the printer the archive is intended to
   * @param operationName name of the operation this archive is about
   *
   * @throws NullPointerException if an argument is null
   * @throws IllegalArgumentException if the given arguments lead to a invalid filename,
   *                ie one that does not match the expected format
   */
  public PrinterArchiveFileName(String printerName, String operationName) {
    this(printerName, operationName, null);
  }

  private static String format(String... groups) {
    return "printer-archive_" + String.join("_", groups) + "." + EXTENSION;
  }

  /**
   * Parses a String (supposedly a "printer archive" filename) to retrieve all contained metadata.
   *
   * @param filename the name to parse
   * @return a {@code PrinterArchiveFileName} with access to every contained metadata
   * @throws IllegalArgumentException if the name does not conform to the expected format
   */
  public static PrinterArchiveFileName parse(String filename) {
    final Matcher matcher = PATTERN.matcher(filename);
    if ( ! matcher.matches()) {
      throw new IllegalArgumentException(
          "The name \"" + filename + "\" does not match the expected pattern for a PrinterArchive");
    }

    final String printerName = CaseFormat.fromKebabCase(matcher.group(2));
    final String operationName = CaseFormat.fromKebabCase(matcher.group(1));
    final String creationDate = matcher.group(4);
    return new PrinterArchiveFileName(printerName, operationName,
                                      creationDate == null ? null : LocalDateTime.parse(creationDate, FileNamer.DATE_TIME_FORMATTER));
  }

  public String getPrinterName() {
    return printerName;
  }

  public String getOperationName() {
    return operationName;
  }

  public Optional<LocalDateTime> getCreationDate() {
    return Optional.ofNullable(creationDate);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PrinterArchiveFileName that = (PrinterArchiveFileName) o;
    return Objects.equals(fileName, that.fileName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(fileName);
  }

  /**
   * Returns a {@code String} representation of this object, <i>ie</i> the formatted filename.
   *
   * @return the filename formatted as a string
   */
  @Override
  public String toString() {
    return fileName;
  }
}
